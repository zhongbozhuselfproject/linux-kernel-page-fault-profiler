#!/bin/bash
nice ./work 1024 R 50000 & nice ./work 1024 R 10000 &

for job in `jobs -p`
do
echo $job
    wait $job
done

./monitor > profile_case1_1.data

sleep 1

nice ./work 1024 R 50000 & nice ./work 1024 L 10000 &

for job in `jobs -p`
do
echo $job
    wait $job
done

./monitor > profile_case1_2.data