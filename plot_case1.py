import matplotlib.pyplot as plt

filenames = ["profile_case1_1.data", "profile_case1_2.data"]

page_fault_cnt = 0
plotx = []
ploty = []
t0 = 0
idx = 0

plt.figure(figsize=(10, 6))

for fname in filenames:
    with open(fname, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            line = lines[i]
            line_data = line.strip()
            if line_data=="" or not (line_data[0]>='0' and line_data[0]<='9'):
                # print("skip useless line")
                continue
            sample_time, minor, major, cpu_use = line_data.split()
            if i == 0:
                t0 = int(sample_time)
            page_fault_cnt += int(minor)
            page_fault_cnt += int(major)
            plotx.append(int(sample_time)-t0)
            ploty.append(page_fault_cnt)

    plt.plot(plotx, ploty, label="profile_case1_{dataid}.data".format(dataid=idx+1))
    # plot next one
    idx += 1
    page_fault_cnt = 0
    plotx = []
    ploty = []

plt.legend()
plt.title("profile case 1 with two memory access patterns")
plt.ylabel("accumulated page faults")
plt.xlabel("time in jiffies")
plt.savefig("case1.jpg")