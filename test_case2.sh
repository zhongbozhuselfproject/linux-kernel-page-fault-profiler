#!/bin/bash

START=1

echo "STARTING CASE 2, USING RANGE 1<=N<=15"

for N in {1..15}
do
    echo "TESTING N=$N PROCESSES"
    for (( i=$START; i<=$N; i++ ))
    do
        nice ./work 200 R 10000 &
    done

    for job in `jobs -p`
    do
    echo $job
        wait $job
    done

    ./monitor > "profile_case2_N$N.data"
    sleep 1
done
