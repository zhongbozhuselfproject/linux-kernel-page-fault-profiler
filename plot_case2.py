import matplotlib.pyplot as plt

NUM_CONCURRENCY = 15

filenames = ["profile_case2_N{num}.data".format(num=i+1) for i in range(NUM_CONCURRENCY)]
points_of_interest = [1,5,15]
total_cpu_use = 0
plotx = [i+1 for i in range(NUM_CONCURRENCY)]
ploty = []
idx = 0

fig, ax = plt.subplots(figsize=(10,6))

for fname in filenames:
    numrows = 0
    with open(fname, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            line = lines[i]
            line_data = line.strip()
            if line_data=="" or not (line_data[0]>='0' and line_data[0]<='9'):
                # print("skip useless line")
                continue
            sample_time, minor, major, cpu_use = line_data.split()
            total_cpu_use += int(cpu_use)
            numrows += 1
        print("number of periods is %d for file %s"%(numrows, fname))
        ploty.append(round(total_cpu_use/numrows, 2))

    # plot next one
    idx += 1
    total_cpu_use = 0;

plt.plot(plotx, ploty)
for point in points_of_interest:
    idx = point-1
    plt.plot(point, ploty[idx], marker='o', color="red")
    ax.text(point, ploty[idx], ploty[idx], size=12)
plt.title("profile case 2 with 1~15 concurrent working processes")
plt.ylabel("total cpu use in nanoseconds")
plt.xlabel("concurreny N")
plt.savefig("case2.jpg")
